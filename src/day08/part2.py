import helpers


def print_layer(layer):
    result = ''
    for row in layer.rows:
        row_result = ''
        for pixel_v in row:
            if pixel_v == 1:
                row_result += '#'
            elif pixel_v == 0:
                row_result += '.'
        result += row_result + '\n'
    print(result.strip())


input_file_name = './src/day08/inputs/input'

content = helpers.load_input(input_file_name)

w = 25
h = 6

image = helpers.create_image(w, h, content)

# WAY 1
decoded_layer = helpers.Layer()
for i in range(h):
    row = [-1] * w
    decoded_layer.rows.append(row)

for image_layer in image.layers:
    for row_i, image_layer_row in enumerate(image_layer.rows):
        for pixel_i, image_pixel in enumerate(image_layer_row):
            image_pixel_v = image_pixel.v
            decoded_layer_pixel_v = decoded_layer.rows[row_i][pixel_i]

            if decoded_layer_pixel_v == -1 and image_pixel_v != 2:
                decoded_layer.rows[row_i][pixel_i] = image_pixel_v

# WAY 2
d_l_2 = helpers.Layer()
for i in range(h):
    row = [-1] * w
    d_l_2.rows.append(row)

for i in range(h):
    for j in range(w):
        for layer in image.layers:
            l_v = layer.rows[i][j].v
            if l_v != 2:
                break
        d_l_2.rows[i][j] = l_v

print_layer(decoded_layer)
print()
print_layer(d_l_2)