import helpers

input_file_name = './src/day08/inputs/input'

content = helpers.load_input(input_file_name)

image = helpers.create_image(25, 6, content)

min_0_layer = image.layers[0]
for i, layer in enumerate(image.layers):
    if layer.num_of(0) < min_0_layer.num_of(0):
        min_0_layer = layer

print(min_0_layer.num_of(1) * min_0_layer.num_of(2))