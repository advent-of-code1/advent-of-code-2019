class Pixel:
    def __init__(self, v):
        self.v = v

    def __str__(self):
        return str(self.v)

    def __repr__(self):
        return str(self)


class Layer:
    def __init__(self):
        self.rows = []

    def __str__(self):
        return str(self.rows)

    def __repr__(self):
        return str(self)

    def num_of(self, digit):
        num_of_digit = 0
        for row in self.rows:
            for pixel in row:
                if digit == pixel.v:
                    num_of_digit += 1
        return num_of_digit


class Image:
    def __init__(self, w, h):
        self.layers = []

    def __str__(self):
        return str(self.layers)

    def __repr__(self):
        return str(self)


def load_input(filename):
    with open(filename) as f:
        content = f.read()
    return content.strip()


def create_image(w, h, content):
    pixels_per_layer = w*h
    layers_per_image = len(content) / pixels_per_layer
    if int(layers_per_image) != layers_per_image:
        print('Something goofed! Layers per image should be int.')
    layers_per_image = int(layers_per_image)
    rows_per_layer = h

    # Create empty image
    image = Image(w, h)
    for i in range(layers_per_image):
        layer = Layer()
        for j in range(rows_per_layer):
            layer.rows.append([])
        image.layers.append(layer)

    # Fill empty image with pixels
    for pixel_i in range(len(content)):
        pixel = Pixel(int(content[pixel_i]))

        layer_i = int(pixel_i / pixels_per_layer)
        layer = image.layers[layer_i]
        for row_i, row in enumerate(layer.rows):
            if len(row) < w:
                break

        layer.rows[row_i].append(pixel)

    return image


