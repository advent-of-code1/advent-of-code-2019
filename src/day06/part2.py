import helpers

input_file_name = './src/day06/inputs/input'

input_lines = helpers.load_input(input_file_name)

planets = helpers.create_planets(input_lines)

planet1 = helpers.get_planet_by_id(planets, 'YOU').parent
planet2 = helpers.get_planet_by_id(planets, 'SAN').parent

print(planet1.distance_to(planets, planet2))
