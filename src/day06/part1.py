import helpers

input_file_name = './src/day06/inputs/test_input_1'

input_lines = helpers.load_input(input_file_name)

planets = helpers.create_planets(input_lines)

total_depth = 0
for planet in planets:
    total_depth += planet.depth()
print(total_depth)
