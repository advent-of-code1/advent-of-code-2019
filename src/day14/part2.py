from collections import defaultdict

import helpers

input_file_name = './src/day14/inputs/test_input_3'

input_lines = helpers.load_input(input_file_name)

units = helpers.create_units(input_lines)

nf = helpers.Nanofactory(units)
fuel_unit = nf.unit_by_id('FUEL')

total_ores = 0
current_reqs = [['FUEL', 1]]
extras = defaultdict(int)
while current_reqs:
    # merge items in current reqs
    current_reqs_dict = {}
    for current_req in current_reqs:
        current_req_id = current_req[0]
        current_req_q = current_req[1]
        if current_req_id not in current_reqs_dict.keys():
            current_reqs_dict[current_req_id] = current_req_q
        else :
            current_reqs_dict[current_req_id] += current_req_q
    current_reqs = []
    for current_req_id, current_req_total_q in current_reqs_dict.items():
        current_reqs.append([current_req_id, current_req_total_q])

    print(current_reqs)
    # see whats currently required
    c_req = current_reqs.pop(0)
    print('Taking: {}'.format(c_req))
    c_req_id = c_req[0]
    c_req_q = c_req[1]

    if c_req_id == 'ORE':
        total_ores += c_req_q
        continue

    # find unit with required id
    unit = nf.unit_by_id(c_req_id)

    # calculate how many units are needed
    total_unit_q = extras[c_req_id]
    while total_unit_q < c_req_q:
        current_reqs.extend(unit.requires)
        total_unit_q += unit.q
    extras[c_req_id] = total_unit_q - c_req_q

print('Total ore for one fuel: {}'.format(total_ores))
print('Extras after 1 fuel: {}'.format(extras))

ores_for_1_fuel = total_ores
trillion_ores = 1000000000000
first_iteration_fuel = int(trillion_ores / ores_for_1_fuel)

first_iteration_extras = []
for extra_id, extra_q in extras.items():
    if extra_q > 0:
        first_iteration_extras.append([extra_q, extra_id])
print(first_iteration_fuel)
print(first_iteration_extras)
