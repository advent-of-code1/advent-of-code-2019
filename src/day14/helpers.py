class Nanofactory:
    def __init__(self, units):
        self.units = units

    def __str__(self):
        return '{}'.format(self.units)

    def __repr__(self):
        return str(self)

    def unit_by_id(self, id):
        for unit in self.units:
            if unit.id == id:
                return unit
        return None


class Unit:
    def __init__(self, id, q, requires):
        self.id = id
        self.q = q
        self.requires = requires

    def __str__(self):
        return '{} {}: {}'.format(self.q, self.id, self.requires)

    def __repr__(self):
        return str(self)


def create_units(content):
    units = []
    for line in content:
        requires_part = line.split(' => ')[0]
        unit_part = line.split(' => ')[1]

        unit_id = unit_part.split(' ')[1]
        unit_q = int(unit_part.split(' ')[0])

        requires = []
        requirements = requires_part.split(', ')
        for req in requirements:
            req_id = req.split(' ')[1]
            req_q = int(req.split(' ')[0])
            requires.append([req_id, req_q])

        units.append(
            Unit(
                id=unit_id,
                q=unit_q,
                requires=requires
            )
        )

    return units


def load_input(filename):
    with open(filename) as f:
        content = f.readlines()
    return [x.strip() for x in content]
