import helpers

input_file_name = './src/day05/inputs/input'

input_list = helpers.load_input(input_file_name)

computer = helpers.Computer()
computer.load_commands(input_list)
succ = computer.process_commands()
