import helpers

input_file_name = './src/day11/inputs/input'

input_list = helpers.load_input(input_file_name)


computer = helpers.Computer(pause_on_output=True)
computer.load_commands(input_list)

hull = helpers.Hull()
while True:
    current_color = hull.panel_colors[hull.robot_position]
    computer.load_inputs([current_color])

    res1 = computer.process_commands()
    if res1 == helpers.Computer.HALT:
        break
    res2 = computer.process_commands()
    if res2 == helpers.Computer.HALT:
        break

    color_to_paint = computer.outputs[-2]
    direction_to_turn = computer.outputs[-1]
    hull.paint_panel(color_to_paint)
    hull.turn_robot(direction_to_turn)
    hull.move_robot()

print(len(hull.panel_colors.keys()))
