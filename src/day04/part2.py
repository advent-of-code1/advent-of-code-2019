import helpers

input_file_name = './src/day04/inputs/input'

start, end = helpers.load_input(input_file_name)

possible_pwd_cnt = 0
for num in range(start, end+1):
    if helpers.Password(num).is_valid_part_2():
        possible_pwd_cnt += 1

print(possible_pwd_cnt)
