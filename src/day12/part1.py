import helpers

input_file_name = './src/day12/inputs/input'

input_content = helpers.load_input(input_file_name)

moons = helpers.create_moons(input_content)

jupiter = helpers.Jupiter(moons)

step = 0
while True:
    jupiter.apply_gravities()
    jupiter.apply_velocities()
    step += 1

    if step == 1000:
        print('Step: {}'.format(step))
        jupiter.nice_print()
        print()
        break
