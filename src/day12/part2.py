import math

import helpers


def compute_lcm(x, y):
    return abs(x*y) // math.gcd(x, y)


input_file_name = './src/day12/inputs/input'

input_content = helpers.load_input(input_file_name)

steps_for_plane = {}
for plane in ['x', 'y', 'z']:
    moons = helpers.create_moons(input_content)

    jupiter = helpers.Jupiter(moons)

    previous_jupiters = []

    step = 0
    while True:
        if step % 1000 == 0:
            print('Step: {}'.format(step))
        previous_jupiters.append(jupiter.hashes_by_plane(plane))
        jupiter.apply_gravities()
        jupiter.apply_velocities()
        step += 1

        if jupiter.hashes_by_plane(plane) in previous_jupiters:
            steps_for_plane[plane] = step
            print('Step: {} <- {}'.format(step, plane))
            break

print(steps_for_plane)
lcm1 = compute_lcm(steps_for_plane['x'], steps_for_plane['y'])
lcm2 = compute_lcm(lcm1, steps_for_plane['z'])
print(lcm2)
