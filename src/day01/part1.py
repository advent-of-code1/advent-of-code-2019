import helpers

input_file_name = './src/day01/inputs/input'

content = helpers.load_input(input_file_name)

total_fuel = 0
for module_mass in content:
    total_fuel += helpers.fuel_from_mass(module_mass)

print(total_fuel)
