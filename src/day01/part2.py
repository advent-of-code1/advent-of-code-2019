import helpers

input_file_name = './src/day01/inputs/input'


content = helpers.load_input(input_file_name)

total_fuel = 0
for module_mass in content:
    fuel_mass = helpers.fuel_from_mass(module_mass)
    while fuel_mass > 0:
        total_fuel += fuel_mass
        fuel_mass = helpers.fuel_from_mass(fuel_mass)

print(total_fuel)
