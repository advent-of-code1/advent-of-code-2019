def load_input(filename):
    with open(filename) as f:
        content = f.readlines()
    return [int(x.strip()) for x in content]


def fuel_from_mass(mass):
    return int(mass/3)-2
