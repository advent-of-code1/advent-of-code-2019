import helpers

input_file_name = './src/day10/inputs/input'

input_content = helpers.load_input(input_file_name)

map = helpers.create_map(input_content)

map.calculate_asteroids_sees()

max_sees_asteroid = map.asteroids[0]
for asteroid in map.asteroids:
    if len(asteroid.sees) > len(max_sees_asteroid.sees):
        max_sees_asteroid = asteroid

print('{}: sees {}'.format(max_sees_asteroid, len(max_sees_asteroid.sees)))

map.draw_asteroid(max_sees_asteroid)
