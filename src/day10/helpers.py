import math
import sys


class Map:
    def __init__(self, w, h):
        self.w = w
        self.h = h
        self.asteroids = []

    def is_between(self, asteroid1, asteroid2, asteroid):
        # is asteroid between asteroid1 and asteroid2
        crossproduct = (asteroid.y - asteroid1.y) * (asteroid2.x - asteroid1.x) - (asteroid.x - asteroid1.x) * (asteroid2.y - asteroid1.y)

        # compare versus epsilon for floating point values, or != 0 if using integers
        if abs(crossproduct) > sys.float_info.epsilon:
            return False

        dotproduct = (asteroid.x - asteroid1.x) * (asteroid2.x - asteroid1.x) + (asteroid.y - asteroid1.y) * (asteroid2.y - asteroid1.y)
        if dotproduct < 0:
            return False

        squaredlengthba = (asteroid2.x - asteroid1.x) * (asteroid2.x - asteroid1.x) + (asteroid2.y - asteroid1.y) * (asteroid2.y - asteroid1.y)
        if dotproduct > squaredlengthba:
            return False

        return True

    def does_see(self, asteroid1, asteroid2):
        for asteroid in self.asteroids:
            if asteroid != asteroid1 and asteroid != asteroid2:
                is_between = self.is_between(asteroid1, asteroid2, asteroid)
                if is_between:
                    return False
        return True

    def sees(self, asteroid):
        seen_asteroids = []
        for other_asteroid in self.asteroids:
            if other_asteroid != asteroid:
                if self.does_see(asteroid, other_asteroid):
                    seen_asteroids.append(other_asteroid)
        return seen_asteroids

    def calculate_asteroids_sees(self):
        for i, asteroid in enumerate(self.asteroids, 1):
            asteroid_sees = self.sees(asteroid)
            asteroid.sees = asteroid_sees
            #print('Calculated sees: {}/{}'.format(i, len(self.asteroids)))

    def get_asteroid_by_coords(self, x, y):
        for asteroid in self.asteroids:
            if asteroid.x == x and asteroid.y == y:
                return asteroid
        return None

    def get_all_asteroids_between(self, asteroid1, asteroid2):
        all_asteroids_between = []
        for asteroid in self.asteroids:
            if asteroid != asteroid1 and asteroid != asteroid2:
                if self.is_between(asteroid1, asteroid2, asteroid):
                    all_asteroids_between.append(asteroid)
        return all_asteroids_between

    def distance(self, asteroid1, asteroid2):
        return math.sqrt(((asteroid1.x - asteroid2.x) ** 2) + ((asteroid1.y - asteroid2.y) ** 2))

    def destroy_asteroid(self, asteroid):
        self.asteroids.remove(asteroid)

    def draw(self):
        result = ''
        for y in range(self.h):
            for x in range(self.w):
                map_asteroid = self.get_asteroid_by_coords(x, y)
                if map_asteroid:
                    result += '#'
                else:
                    result += '.'
            result += '\n'
        print(result.strip())

    def draw_asteroid(self, asteroid):
        result = ''
        for y in range(self.h):
            for x in range(self.w):
                map_asteroid = self.get_asteroid_by_coords(x, y)
                if map_asteroid == asteroid:
                    result += 'X'
                elif map_asteroid in asteroid.sees:
                    result += 'O'
                elif map_asteroid:
                    result += '#'
                else:
                    result += '.'
            result += '\n'
        print(result.strip())

    def __str__(self):
        return str(self.asteroids)

    def __repr__(self):
        return str(self)


class Asteroid:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.sees = None

    def __str__(self):
        return '({}, {})'.format(self.x, self.y)

    def __repr__(self):
        return str(self)


def load_input(filename):
    with open(filename) as f:
        content = f.readlines()
    return [x.strip() for x in content]


def create_map(content):
    map = Map(w=len(content[0]), h=len(content))

    for y, line in enumerate(content):
        for x, char in enumerate(line):
            if char == '#':
                map.asteroids.append(
                    Asteroid(x, y)
                )
    return map