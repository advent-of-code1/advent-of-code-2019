import math

import helpers


def clockwiseangle_and_distance(point):
    # Vector between point and the origin: v = p - o
    vector = [point.x-origin.x, point.y-origin.y]
    # Length of vector: ||v||
    lenvector = math.hypot(vector[0], vector[1])
    # If length is zero there is no angle
    if lenvector == 0:
        return -math.pi, 0
    # Normalize vector: v/||v||
    normalized = [vector[0]/lenvector, vector[1]/lenvector]
    dotprod  = normalized[0]*refvec[0] + normalized[1]*refvec[1]     # x1*x2 + y1*y2
    diffprod = refvec[1]*normalized[0] - refvec[0]*normalized[1]     # x1*y2 - y1*x2
    angle = math.atan2(diffprod, dotprod)
    # Negative angles represent counter-clockwise angles so we need to subtract them
    # from 2*pi (360 degrees)
    if angle < 0:
        return 2*math.pi+angle, lenvector
    # I return first the angle because that's the primary sorting criterium
    # but if two vectors have the same angle then the shorter distance should come first.
    return angle, lenvector


def order_asteroids_clockwise(asteroids):
    sorted_asteroids = sorted(asteroids, key=clockwiseangle_and_distance)
    if sorted_asteroids[0].x == max_sees_asteroid.x:
        return [sorted_asteroids[0]] + list(reversed(sorted_asteroids[1:]))
    else:
        return list(reversed(sorted_asteroids))


input_file_name = './src/day10/inputs/input'

input_content = helpers.load_input(input_file_name)

map = helpers.create_map(input_content)

map.calculate_asteroids_sees()

max_sees_asteroid = map.asteroids[0]
for asteroid in map.asteroids:
    if len(asteroid.sees) > len(max_sees_asteroid.sees):
        max_sees_asteroid = asteroid

print('Station at: {}'.format(max_sees_asteroid))

origin = max_sees_asteroid
refvec = [0, -1]

rotation = 0
destroyed_asteroids = []
while True:
    #print('Rotation: {}'.format(rotation))
    #map.draw_asteroid(max_sees_asteroid)
    if len(map.asteroids) == 1:
        break
    clockwise_asteroids = order_asteroids_clockwise(max_sees_asteroid.sees)
    #print('Destroying:', clockwise_asteroids)
    for asteroid in clockwise_asteroids:
        map.destroy_asteroid(asteroid)
        destroyed_asteroids.append(asteroid)

        if len(destroyed_asteroids) == 200:
            print(destroyed_asteroids[-1].x*100 + destroyed_asteroids[-1].y)

    map.calculate_asteroids_sees()
    rotation += 1

