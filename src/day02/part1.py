import helpers

input_file_name = './src/day02/inputs/input'

input_list = helpers.load_input(input_file_name)

output = helpers.calculate_output(input_list, 12, 2)

print(output)
