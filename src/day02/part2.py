import helpers

input_file_name = './src/day02/inputs/input'

desired_output = 19690720

for noun in range(0, 100):
    for verb in range(0, 100):
        input_list = helpers.load_input(input_file_name)

        output = helpers.calculate_output(input_list, noun, verb)

        if output == desired_output:
            print(100 * noun + verb)
            exit()
