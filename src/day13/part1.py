import helpers

input_file_name = './src/day13/inputs/input'

input_list = helpers.load_input(input_file_name)

computer = helpers.Computer()
computer.load_commands(input_list)

res1 = computer.process_commands()

print(computer.outputs)

game = helpers.Game(computer.outputs)

block_tiles_count = 0
for tile in game.tiles.values():
    if tile == game.BLOCK:
        block_tiles_count += 1
print(block_tiles_count)
