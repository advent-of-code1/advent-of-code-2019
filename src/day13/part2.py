import helpers

input_file_name = './src/day13/inputs/input'

input_list = helpers.load_input(input_file_name)

computer = helpers.Computer()
computer.load_commands(input_list)
computer.change_cmds({0: 2})

res1 = computer.process_commands()
print(computer.game.score)
