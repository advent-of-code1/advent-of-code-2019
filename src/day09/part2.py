import time

import helpers

input_file_name = './src/day09/inputs/input'

input_list = helpers.load_input(input_file_name)

start_time = time.time()

computer = helpers.Computer()
computer.load_commands(input_list)
computer.load_inputs([2])
succ = computer.process_commands()
print('Execution time: {}s'.format(time.time()-start_time))
print(computer.outputs)