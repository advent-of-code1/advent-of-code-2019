import helpers

input_file_name = './src/day09/inputs/input'

input_list = helpers.load_input(input_file_name)

computer = helpers.Computer()
computer.load_commands(input_list)
computer.load_inputs([1])
succ = computer.process_commands()
print(computer.outputs)
