import itertools

import helpers

def test_phase_settings(one_phase_settings):
    amplifiers = []
    for i in range(5):
        computer = helpers.Computer()
        computer.load_commands(input_list)

        amplifiers.append(computer)

    counter = 0
    first_iteration = True
    while True:
        if counter == 5:
            first_iteration = False
        current_index = counter % 5
        #print('Amplifier: {}'.format(current_index))
        previous_index = counter % 5 - 1 if current_index != 0 else 4

        amplifier = amplifiers[current_index]

        first_input = one_phase_settings[current_index]
        second_input = amplifiers[previous_index].outputs[-1] if amplifiers[previous_index].outputs else 0

        if first_iteration:
            current_inputs = [first_input, second_input]
        else:
            current_inputs = [second_input]


        #print('\tCurrent position: {}'.format(amplifier.current_position))
        #print('\tCommands: {}'.format(amplifier.commands))
        #print('\tInputs: {}'.format(current_inputs))

        amplifier.load_inputs(current_inputs)
        succ = amplifier.process_commands()
        #print('\tProcessing commands...')
        #print('\tCurrent position: {}'.format(amplifier.current_position))
        #print('\tCommands: {}'.format(amplifier.commands))
        #print('\tOutputs: {}'.format(amplifier.outputs))
        if succ == True and amplifier == amplifiers[-1]:
            return amplifiers[-1].outputs[-1]

        counter += 1

input_file_name = './src/day07/inputs/input'

input_list = helpers.load_input(input_file_name)

phase_settings_nums = [5, 6, 7, 8, 9]

all_phase_settings = [list(one_phase_settings) for one_phase_settings in list(itertools.permutations(phase_settings_nums))]

final_outputs = {}
for one_phase_settings in all_phase_settings:
    try:
        result = test_phase_settings(one_phase_settings)
    except Exception as e:
        result = 0
    final_outputs[tuple(one_phase_settings)] = result
    print('{} : {}'.format(tuple(one_phase_settings), result))


print(max(final_outputs.values()))
