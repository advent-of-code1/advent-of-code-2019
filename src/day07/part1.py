import itertools

import helpers

input_file_name = './src/day07/inputs/input'

input_list = helpers.load_input(input_file_name)

phase_settings_nums = [0, 1, 2, 3, 4]

all_phase_settings = [list(one_phase_settings) for one_phase_settings in list(itertools.permutations(phase_settings_nums))]

final_outputs = {}
for one_phase_settings in all_phase_settings:

    amplifiers = []
    for i in range(5):
        computer = helpers.Computer()
        computer.load_commands(input_list)

        current_output = 0 if i == 0 else amplifiers[i-1].outputs[0]
        current_inputs = [one_phase_settings[i], current_output]

        computer.load_inputs(current_inputs)
        succ = computer.process_commands()

        amplifiers.append(computer)

    final_outputs[tuple(one_phase_settings)] = amplifiers[-1].outputs[0]

print(max(final_outputs.values()))
