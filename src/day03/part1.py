import helpers


input_file_name = './src/day03/inputs/input'

wire1, wire2 = helpers.load_input(input_file_name)

# wire1, wire2 = ['R8', 'U5', 'L5', 'D3'], ['U7', 'R6', 'D4', 'L4']
# wire1, wire2 = ['R75', 'D30', 'R83', 'U83', 'L12', 'D49', 'R71', 'U7', 'L72'], ['U62', 'R66', 'U55', 'R34', 'D71', 'R55', 'D58', 'R83']
# wire1, wire2 = ['R98', 'U47', 'R26', 'D63', 'R33', 'U87', 'L62', 'D20', 'R33', 'U53', 'R51'], ['U98', 'R91', 'D20', 'R16', 'D67', 'R40', 'U7', 'R15', 'U6', 'R7']

wire_1_lines = helpers.get_lines(wire1)
wire_2_lines = helpers.get_lines(wire2)

intersections = helpers.find_intersections(wire_1_lines, wire_2_lines)

min_distance = min([intersection.distance_from_0_0 for intersection in intersections])

print(min_distance)
